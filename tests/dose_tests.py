import json
import os
from unittest import TestCase

from openapscontrib.dose.dose import calculate_doses


def get_file_at_path(path):
    return '{}/{}'.format(os.path.dirname(os.path.realpath(__file__)), path)


class RecommendTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(RecommendTestCase, cls).setUpClass()

        with open(get_file_at_path('fixtures/read_bg_targets.json')) as fp:
            cls.bg_targets = json.load(fp)

        with open(get_file_at_path('fixtures/read_insulin_sensitivies.json')) as fp:
            cls.insulin_sensitivities = json.load(fp)

        with open(get_file_at_path('fixtures/read_selected_basal_profile.json')) as fp:
            cls.basal_profile = json.load(fp)

        with open(get_file_at_path('fixtures/read_settings.json')) as fp:
            cls.settings_percent = json.load(fp)

        with open(get_file_at_path('fixtures/read_settings_absolute.json')) as fp:
            cls.settings_absolute = json.load(fp)

    def test_no_change(self):
        glucose = [
            {'date': '2015-07-19T20:00:00', 'amount': 100}
        ]

        history = []

        clock = '2015-07-19T16:00:00'

        self.assertEqual([], calculate_doses(
            glucose,
            resolved_history=history,
            settings=self.settings_percent,
            clock=clock,
            bg_targets=self.bg_targets,
            insulin_sensitivities=self.insulin_sensitivities,
            basal_profile=self.basal_profile
        ))

    def test_start_high_end_in_range(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'amount': 200},
            {'date': '2015-07-19T18:30:00', 'amount': 180},
            {'date': '2015-07-19T19:00:00', 'amount': 150},
            {'date': '2015-07-19T19:30:00', 'amount': 120},
            {'date': '2015-07-19T20:00:00', 'amount': 100}
        ]

        history = []

        clock = '2015-07-19T16:00:00'

        self.assertEqual([], calculate_doses(
            glucose,
            resolved_history=history,
            settings=self.settings_percent,
            clock=clock,
            bg_targets=self.bg_targets,
            insulin_sensitivities=self.insulin_sensitivities,
            basal_profile=self.basal_profile
        ))

    def test_start_low_end_in_range(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'glucose': 60},
            {'date': '2015-07-19T18:30:00', 'glucose': 70},
            {'date': '2015-07-19T19:00:00', 'glucose': 80},
            {'date': '2015-07-19T19:30:00', 'glucose': 90},
            {'date': '2015-07-19T20:00:00', 'glucose': 100}
        ]

        history = []

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 0.0,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 0.0,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_correct_low_at_min(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'glucose': 100},
            {'date': '2015-07-19T18:30:00', 'glucose': 90},
            {'date': '2015-07-19T19:00:00', 'glucose': 85},
            {'date': '2015-07-19T19:30:00', 'glucose': 90},
            {'date': '2015-07-19T20:00:00', 'glucose': 100}
        ]

        history = [
            {
                'type': 'TempBasal',
                'start_at': '2015-07-19T12:55:00',
                'end_at': '2015-07-19T13:25:00',
                'amount': 38,
                'unit': 'percent'
            }
        ]

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 17.0,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 0.125,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_continue_existing_temp_basal(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'amount': 100},
            {'date': '2015-07-19T18:30:00', 'amount': 90},
            {'date': '2015-07-19T19:00:00', 'amount': 85},
            {'date': '2015-07-19T19:30:00', 'amount': 90},
            {'date': '2015-07-19T20:00:00', 'amount': 100}
        ]

        history = [
            {
                'type': 'TempBasal',
                'start_at': '2015-07-19T15:55:00',
                'end_at': '2015-07-19T16:25:00',
                'amount': 17,
                'unit': 'percent'
            }
        ]

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        history = [
            {
                'type': 'TempBasal',
                'start_at': '2015-07-19T15:55:00',
                'end_at': '2015-07-19T16:25:00',
                'amount': 0.125,
                'unit': 'U/hour'
            }
        ]

        self.assertListEqual(
            [],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_extend_existing_temp_basal(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'glucose': 100},
            {'date': '2015-07-19T18:30:00', 'glucose': 90},
            {'date': '2015-07-19T19:00:00', 'glucose': 85},
            {'date': '2015-07-19T19:30:00', 'glucose': 90},
            {'date': '2015-07-19T20:00:00', 'glucose': 100}
        ]

        history = [
            {
                'type': 'TempBasal',
                'start_at': '2015-07-19T15:35:00',
                'end_at': '2015-07-19T16:05:00',
                'amount': 17,
                'unit': 'percent'
            }
        ]

        clock = '2015-07-19T15:59:20'

        self.assertListEqual(
            [
                {
                    'duration': 0,
                    'rate': 100,
                    'temp': 'percent',
                    'type': 'TempBasal'
                },
                {
                    'duration': 30,
                    'rate': 17.0,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        history = [
            {
                'type': 'TempBasal',
                'start_at': '2015-07-19T15:35:00',
                'end_at': '2015-07-19T16:05:00',
                'amount': 0.125,
                'unit': 'U/hour'
            }
        ]

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 0.125,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_cancel_existing_temp_basal(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'glucose': 100},
            {'date': '2015-07-19T18:30:00', 'glucose': 90},
            {'date': '2015-07-19T19:00:00', 'glucose': 80},
            {'date': '2015-07-19T19:30:00', 'glucose': 90},
            {'date': '2015-07-19T20:00:00', 'glucose': 100}
        ]

        history = [
            {
                'type': 'TempBasal',
                'start_at': '2015-07-19T15:55:00',
                'end_at': '2015-07-19T16:25:00',
                'amount': 45,
                'unit': 'percent'
            }
        ]

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [
                {
                    'duration': 0,
                    'rate': 100,
                    'temp': 'percent',
                    'type': 'TempBasal'
                },
                {
                    'duration': 30,
                    'rate': 0.0,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 0.0,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_cancel_existing_temp_basal_with_no_new_dose(self):
        glucose = [
            {'date': '2015-07-19T20:00:00', 'amount': 100}
        ]

        history = [
            {
                'type': 'TempBasal',
                'start_at': '2015-07-19T15:55:00',
                'end_at': '2015-07-19T16:25:00',
                'amount': 45,
                'unit': 'percent'
            }
        ]

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [
                {
                    'duration': 0,
                    'rate': 100,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        self.assertListEqual(
            [
                {
                    'duration': 0,
                    'rate': 0.0,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_start_high_end_low(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'glucose': 200},
            {'date': '2015-07-19T18:30:00', 'glucose': 160},
            {'date': '2015-07-19T19:00:00', 'glucose': 120},
            {'date': '2015-07-19T19:30:00', 'glucose': 80},
            {'date': '2015-07-19T20:00:00', 'glucose': 60}
        ]

        history = []

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 0.0,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 0.0,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_start_low_end_high(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'amount': 60},
            {'date': '2015-07-19T18:30:00', 'amount': 80},
            {'date': '2015-07-19T19:00:00', 'amount': 120},
            {'date': '2015-07-19T19:30:00', 'amount': 160},
            {'date': '2015-07-19T20:00:00', 'amount': 200}
        ]

        history = []

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 0.0,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 0.0,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_start_low_end_in_range_allow_temp_below_range(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'amount': 60},
            {'date': '2015-07-19T18:30:00', 'amount': 70},
            {'date': '2015-07-19T19:00:00', 'amount': 80},
            {'date': '2015-07-19T19:30:00', 'amount': 90},
            {'date': '2015-07-19T20:00:00', 'amount': 100}
        ]

        history = []

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile,
                allow_predictive_temp_below_range=True
            )
        )

        self.assertListEqual(
            [],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile,
                allow_predictive_temp_below_range=True
            )
        )

        history = [
            {
                'type': 'TempBasal',
                'start_at': '2015-07-19T15:55:00',
                'end_at': '2015-07-19T16:25:00',
                'amount': 0.0,
                'unit': 'U/hour'
            }
        ]

        self.assertListEqual(
            [
                {
                    'duration': 0,
                    'rate': 0.0,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile,
                allow_predictive_temp_below_range=True
            )
        )

    def test_start_low_end_high_allow_temp_below_range(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'amount': 60},
            {'date': '2015-07-19T18:30:00', 'amount': 80},
            {'date': '2015-07-19T19:00:00', 'amount': 120},
            {'date': '2015-07-19T19:30:00', 'amount': 160},
            {'date': '2015-07-19T20:00:00', 'amount': 200}
        ]

        history = []

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 100.0,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile,
                allow_predictive_temp_below_range=True
            )
        )

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 0.8,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile,
                allow_predictive_temp_below_range=True
            )
        )

    def test_flat_and_high(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'glucose': 200},
        ]

        history = []

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 200,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 3.5,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_high_and_falling(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'glucose': 240},
            {'date': '2015-07-19T18:30:00', 'glucose': 220},
            {'date': '2015-07-19T19:00:00', 'glucose': 200},
            {'date': '2015-07-19T19:30:00', 'glucose': 160},
            {'date': '2015-07-19T20:00:00', 'glucose': 124}
        ]

        history = []

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 179,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 1.425,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_in_range_and_rising(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'amount': 90},
            {'date': '2015-07-19T18:30:00', 'amount': 100},
            {'date': '2015-07-19T19:00:00', 'amount': 110},
            {'date': '2015-07-19T19:30:00', 'amount': 120},
            {'date': '2015-07-19T20:00:00', 'amount': 125}
        ]

        history = []

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 183,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 1.475,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_high_and_rising(self):
        glucose = [
            {'date': '2015-07-19T18:00:00', 'glucose': 130},
            {'date': '2015-07-19T18:30:00', 'glucose': 140},
            {'date': '2015-07-19T19:00:00', 'glucose': 150},
            {'date': '2015-07-19T19:30:00', 'glucose': 160},
            {'date': '2015-07-19T20:00:00', 'glucose': 170}
        ]

        history = []

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 200,
                    'temp': 'percent',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        self.assertListEqual(
            [
                {
                    'duration': 30,
                    'rate': 2.975,
                    'temp': 'absolute',
                    'type': 'TempBasal'
                }
            ],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

    def test_no_input_glucose(self):
        glucose = []
        history = []

        clock = '2015-07-19T16:00:00'

        self.assertListEqual(
            [],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_percent,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )

        self.assertListEqual(
            [],
            calculate_doses(
                glucose,
                resolved_history=history,
                settings=self.settings_absolute,
                clock=clock,
                bg_targets=self.bg_targets,
                insulin_sensitivities=self.insulin_sensitivities,
                basal_profile=self.basal_profile
            )
        )
