from datetime import timedelta
from dateutil.parser import parse

from openapscontrib.predict.predict import Schedule


TEMP_BASAL_MINUTES = 30

# How to convert from mg/dL (US format) to mmol/L (non-US format)
MMOLL_CONVERT_FACTOR = 1.0 / 18.0


def current_temp_basal(normalized_history, clock_str):
    """

    :param normalized_history:
    :type normalized_history: list[dict]
    :param clock_str:
    :type clock_str: basestring
    :return:
    :rtype: dict|NoneType
    """
    match = filter(
        lambda e: e['type'] == 'TempBasal' and e['end_at'] > clock_str,
        normalized_history
    )

    return match[0] if len(match) > 0 else None


def calculate_temp_basal(glucose, target_glucose, sensf, current_basal, is_percent, max_basal=0.0, strokes=40.0):
    dose_amount = float(glucose - target_glucose) / sensf
    rate = max(0, dose_amount / (TEMP_BASAL_MINUTES / 60.0) + current_basal)

    if is_percent:
        rate = round(min(max_basal, rate / current_basal) * 100)
    else:
        rate = round(min(max_basal, rate) * strokes) / strokes

    return temp_basal(rate, is_percent)


def temp_basal(rate, is_percent, duration=TEMP_BASAL_MINUTES):
    return {
        'type': 'TempBasal',
        'temp': 'percent' if is_percent else 'absolute',
        'duration': duration,
        'rate': rate
    }


def calculate_doses(
    predicted_glucose_without_basal,
    resolved_history=None,
    settings=None,
    clock=None,
    bg_targets=None,
    insulin_sensitivities=None,
    basal_profile=None,
    allow_predictive_temp_below_range=False
):
    assert resolved_history is not None
    assert settings is not None
    assert clock is not None
    assert bg_targets is not None
    assert insulin_sensitivities is not None

    assert bg_targets['units'] in ['mmol/L', 'mg/dL']

    if len(predicted_glucose_without_basal) == 0:
        # TODO: Should we cancel an existing dose here?
        return []

    targets = Schedule(bg_targets['targets'])
    sensitivities = Schedule(insulin_sensitivities['sensitivities'])
    basals = Schedule(basal_profile)

    clock_time = parse(clock).time()

    final_glucose_entry = predicted_glucose_without_basal[-1]
    final_glucose = final_glucose_entry.get('amount') or final_glucose_entry['glucose']
    final_glucose_time = parse(final_glucose_entry['date']).time()

    min_glucose_entry = min(predicted_glucose_without_basal, key=lambda g: g.get('amount') or g['glucose'])
    min_glucose = min_glucose_entry.get('amount') or min_glucose_entry['glucose']
    min_glucose_time = parse(min_glucose_entry['date']).time()

    targets_at_min = targets.at(min_glucose_time)
    target_low_at_min = targets_at_min['low']
    target_high_at_min = targets_at_min['high']
    targets_at_final = targets.at(final_glucose_time)
    target_low_at_final = targets_at_final['low']
    target_high_at_final = targets_at_final['high']

    if bg_targets['units'] == 'mmol/L':
        target_low_at_min /= MMOLL_CONVERT_FACTOR
        target_high_at_min /= MMOLL_CONVERT_FACTOR
        target_low_at_final /= MMOLL_CONVERT_FACTOR
        target_high_at_final /= MMOLL_CONVERT_FACTOR

    dose = []
    is_percent = settings['temp_basal']['type'] == 'Percent'  # "Units/hour"
    max_basal = 2.0 if is_percent else settings['maxBasal']
    current_basal = basals.at(clock_time)['rate']

    if min_glucose < target_low_at_min and \
            (not allow_predictive_temp_below_range or final_glucose <= target_low_at_final):
        dose.append(calculate_temp_basal(
            min_glucose,
            (target_low_at_min + target_high_at_min) / 2.0,
            sensitivities.at(clock_time)['sensitivity'],
            current_basal,
            is_percent,
            max_basal=max_basal
        ))
    elif final_glucose > target_high_at_final:
        if min_glucose < target_low_at_min:
            max_basal = 1.0 if is_percent else min(max_basal, current_basal)

        dose.append(calculate_temp_basal(
            final_glucose,
            (target_low_at_final + target_high_at_final) / 2.0,
            sensitivities.at(clock_time)['sensitivity'],
            current_basal,
            is_percent,
            max_basal=max_basal
        ))

    current_temp = current_temp_basal(resolved_history, clock)

    if current_temp:
        if dose and \
                dose[-1]['rate'] == current_temp['amount'] and \
                parse(current_temp['end_at']) - parse(clock) > timedelta(minutes=11):
            # Ignore the dose if the current dose is the same rate and has more than 10 minutes remaining
            dose = []
        elif is_percent:
            # If we're changing a percent dose in-progress, we have to cancel it first.
            dose.insert(0, temp_basal(100, is_percent, duration=0))
        elif not dose:
            # If we prefer to not have a dose, cancel the one in progress
            dose.insert(0, temp_basal(0.0, is_percent, duration=0))

    return dose
